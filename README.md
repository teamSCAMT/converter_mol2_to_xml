# Converter_mol2_to_xml

Additional converter for working with programs Narupa Builder and NarupaXR



Input (Ethane.mol2)


```
@<TRIPOS>MOLECULE
narupa builder molecule
8 7 0 0 0
SMALL
GASTEIGER

@<TRIPOS>ATOM
1 C1 -6,165 0,925 -6,713 C.3 1 N1
2 C2 -5,555 0,741 -5,798 C.3 1 N1
3 H1 -7,070 0,516 -6,599 H 1 N1
4 H2 -5,711 0,516 -7,504 H 1 N1
5 H3 -6,261 1,910 -6,857 H 1 N1
6 H4 -4,737 0,221 -6,042 H 1 N1
7 H5 -6,096 0,221 -5,137 H 1 N1
8 H6 -5,287 1,615 -5,394 H 1 N1

@<TRIPOS>BOND
1 2 1 1
2 8 2 1
3 7 2 1
4 6 2 1
5 5 1 1
6 4 1 1
7 3 1 1
```

Output (Ethane.xml)

```
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<Simulation Name="  ">
    <SystemProperties>
        <SimulationBoundary MinimumBoxVolume="2" SimulationBox="2 2 2"/>
        <Thermostat BerendsenCoupling="0.003" EquilibriumTemperature="200" MaximumTemperature="10000" Type="BerendsenThermostat"/>
        <Integrator TimeStep="0.001" Type="VelocityVerlet"/>
    </SystemProperties>
    <Topology>
        <Templates>
            <Residue Name="  ">
                <Atoms>
                    <Atom Element="Carbone" Position="-0.6165,0.0925,-0.6713"/>
                    <Atom Element="Carbone" Position="-0.5555,0.0741,-0.5798"/>
                    <Atom Element="Hydrogen" Position="-0.7070,0.0516,-0.6599"/>
                    <Atom Element="Hydrogen" Position="-0.5711,0.0516,-0.7504"/>
                    <Atom Element="Hydrogen" Position="-0.6261,0.1910,-0.6857"/>
                    <Atom Element="Hydrogen" Position="-0.4737,0.0221,-0.6042"/>
                    <Atom Element="Hydrogen" Position="-0.6096,0.0221,-0.5137"/>
                    <Atom Element="Hydrogen" Position="-0.5287,0.1615,-0.5394"/>
                </Atoms>
                <Bonds>
                    <Bond A="0" B="1"/>
                    <Bond A="1" B="7"/>
                    <Bond A="1" B="6"/>
                    <Bond A="1" B="5"/>
                    <Bond A="0" B="4"/>
                    <Bond A="0" B="3"/>
                    <Bond A="0" B="2"/>
                </Bonds>
            </Residue>
            <ForceFields>
                <InteractiveGaussianForceField GradientScaleFactor="200"/>
                <MM3ForceField>
                    <MM3AtomMappings>
                        <MM3AtomMapping AtomPath="0" Type=" "/>
                        <MM3AtomMapping AtomPath="1" Type=" "/>
                        <MM3AtomMapping AtomPath="2" Type=" "/>
                        <MM3AtomMapping AtomPath="3" Type=" "/>
                        <MM3AtomMapping AtomPath="4" Type=" "/>
                        <MM3AtomMapping AtomPath="5" Type=" "/>
                        <MM3AtomMapping AtomPath="6" Type=" "/>
                        <MM3AtomMapping AtomPath="7" Type=" "/>
                    </MM3AtomMappings>
                </MM3ForceField>
                <LennardJonesForceField>
                    <LennardJonesAtomMappings>
                        <LennardJonesAtomMapping AtomPath="0" MM3Type=" "/>
                        <LennardJonesAtomMapping AtomPath="1" MM3Type=" "/>
                        <LennardJonesAtomMapping AtomPath="2" MM3Type=" "/>
                        <LennardJonesAtomMapping AtomPath="3" MM3Type=" "/>
                        <LennardJonesAtomMapping AtomPath="4" MM3Type=" "/>
                        <LennardJonesAtomMapping AtomPath="5" MM3Type=" "/>
                        <LennardJonesAtomMapping AtomPath="6" MM3Type=" "/>
                        <LennardJonesAtomMapping AtomPath="7" MM3Type=" "/>
                    </LennardJonesAtomMappings>
                </LennardJonesForceField>
            </ForceFields>
        </Templates>
        <Spawners>
            <Spawner Count=" " Name="   " Template="   "/>
        </Spawners>
    </Topology>
</Simulation>
```
