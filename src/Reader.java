package src;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.List;

class Reader {

    public LinkedHashMap<String,String> DataOfAtoms;
    public LinkedHashMap<String,String> DataOfBonds;
    public Integer CountOfAtoms;

    public Reader(Path path) throws IOException {
        int countOfAtoms = 0;
        int countOfBonds = 0;
        String s;
        String[] subStr;
        LinkedHashMap<String,String> atoms = new LinkedHashMap<>();
        List<String> data = Files.readAllLines(path);
        for(int i=0;i<data.size();i++){
            if(data.get(i).equals("narupa builder molecule")){
                String atomsAndBonds = data.get(i+1);
                String[] atomsAndBondsChar = atomsAndBonds.split(" ");
                countOfAtoms = Integer.parseInt(atomsAndBondsChar[0]);
                countOfBonds = Integer.parseInt(atomsAndBondsChar[1]);
            }
        }
        this.CountOfAtoms = countOfAtoms;

        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).equals("@<TRIPOS>ATOM")) {
                int j = i+1;
                for(int cA = 0; cA < countOfAtoms; cA++){
                    s = data.get(j);
                    subStr = s.split(" ");

                    char[] s1 = subStr[1].toCharArray();
                    String subStr2 = subStr[2].replace(',','.');
                    String subStr3 = subStr[3].replace(',','.');
                    String subStr4 = subStr[4].replace(',','.');
                    float floatStr2 = Float.parseFloat(subStr2);
                    floatStr2 /=  10;
                    float floatStr3 = Float.parseFloat(subStr3);
                    floatStr3 /=  10;
                    float floatStr4= Float.parseFloat(subStr4);
                    floatStr4 /=  10;
                    String subStr2ToS = String.format("%.4f",floatStr2);
                    String subStr3ToS = String.format("%.4f",floatStr3);
                    String subStr4ToS = String.format("%.4f",floatStr4);
                    String x = subStr2ToS.replace(",",".");
                    String y = subStr3ToS.replace(",",".");
                    String z = subStr4ToS.replace(",",".");
                    atoms.put(x+","+y+","+z, Beliviks.definition(s1));
                    j++;
                }

            }
        }
        this.DataOfAtoms = atoms;

        LinkedHashMap<String,String> bondsMap = new LinkedHashMap<>();
        String bondsString;
        String[] bondsChar;
        List<String> bonds = Files.readAllLines(path);
        for (int i = 0; i < bonds.size(); i++) {
            if (bonds.get(i).equals("@<TRIPOS>BOND")) {
                int j = i + 1;
                for(int k = 0; k < countOfBonds;  k++){
                    bondsString = (bonds.get(j));
                    bondsChar = bondsString.split(" ");
                    int bondsA = Integer.parseInt(bondsChar[2]);
                    int bondsB = Integer.parseInt(bondsChar[1]);
                    bondsA -= 1;
                    bondsB -= 1;
                    String  bondsAB = Integer.toString(bondsA);
                    bondsAB += " ";
                    bondsAB += Integer.toString(bondsB);
                    String bondsCharToStr = bondsChar[0];
                    bondsMap.put(bondsCharToStr, bondsAB);
                    j++;
                }
            }
        }
        this.DataOfBonds = bondsMap;
    }
}
